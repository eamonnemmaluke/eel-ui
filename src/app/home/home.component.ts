import { Component, OnInit } from '@angular/core';
import { StratService } from '../service/strat.service';
import { Strat } from '../model/strat';
import { Observable, timer, Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private updateSubscription: Subscription;
  strategy=[{ticker:'GOOGL',size:200,target:10, actual:14,status:'Finished- up'}]
  strats: Strat[];
  constructor(private stratService: StratService) { 
 
  }

  ngOnInit() {
    this.updateStrats();
  }
  
  private updateStrats() {
    this.stratService.findAll().subscribe(data => {
        this.strats = data.reverse();
        console.log("loading strategies: ");
        console.log(this.strats[1].id);
        console.log(this.strats);
        this.startRefreshTimer();
      });
    }
      private startRefreshTimer() {
        this.updateSubscription = timer(10000).subscribe(
          (val) => { this.updateStrats() }
        );
      }

}
