import { Component, OnInit } from '@angular/core';
import { Observable, timer, Subscription } from 'rxjs';
  import { Trade } from '../model/trade';
  import { dummy } from '../model/dummy';
  import { TradeService } from '../service/trade.service';
  

// @Component({
//   selector: 'app-trades',
//   templateUrl: './trades.component.html',
//   styleUrls: ['./trades.component.css']
// })
// export class TradesComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }
  
  @Component({
    selector: 'app-trades',
    templateUrl: './trades.component.html',
    styleUrls: ['./trades.component.css']
  })
  export class TradesComponent implements OnInit {
  
    private updateSubscription: Subscription;
    
    trades: Trade[];
    test1= 'a';
  
    constructor(private tradeService: TradeService) { }
  
    ngOnInit() {
      this.updateTrades();
    }
  
    private updateTrades() {
    this.tradeService.findAll().subscribe(data => {
        this.trades = data.reverse();
        console.log("loading trades: ");
        console.log(this.trades);
        this.startRefreshTimer();
      });
    }
  
    private startRefreshTimer() {
      this.updateSubscription = timer(10000).subscribe(
        (val) => { this.updateTrades() }
      );
    }
  }
  
