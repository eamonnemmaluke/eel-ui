import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Trades2Component } from './trades2.component';

describe('Trades2Component', () => {
  let component: Trades2Component;
  let fixture: ComponentFixture<Trades2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Trades2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Trades2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
