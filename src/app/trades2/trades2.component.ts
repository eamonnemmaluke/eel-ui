import { Observable, timer, Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Trade } from '../model/trade';
import { dummy } from '../model/dummy';
import { TradeService } from '../service/trade.service';

@Component({
  selector: 'app-trades2',
  templateUrl: './trades2.component.html',
  styleUrls: ['./trades2.component.css']
})
export class Trades2Component implements OnInit {



  private updateSubscription: Subscription;
  ABC: dummy={id:1,state:'a'};
  tradesb: Trade={id:1,price:2,size:3,stoca_tickerk:'a',lastStateChange: null, tradeType:'c',state:'d'};
  trades: Trade[];

  constructor(private tradeService: TradeService) { }

  ngOnInit() {
    this.updateTrades();
  }

  ngOnDestroy() {
      this.updateSubscription.unsubscribe();
  }


  private updateTrades() {
  this.tradeService.findAll().subscribe(data => {
      this.trades = data.reverse();
      console.log("loading trades: ");
      console.log(this.trades);
      this.startRefreshTimer();
    });
  }

  private startRefreshTimer() {
    this.updateSubscription = timer(10000).subscribe(
      (val) => { this.updateTrades() }
    );
  }
}
