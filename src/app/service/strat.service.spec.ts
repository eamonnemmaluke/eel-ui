import { TestBed } from '@angular/core/testing';

import { StratService } from './strat.service';

describe('StratService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StratService = TestBed.get(StratService);
    expect(service).toBeTruthy();
  });
});
