import { TestBed } from '@angular/core/testing';

import { StratnewService } from './stratnew.service';

describe('StratnewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StratnewService = TestBed.get(StratnewService);
    expect(service).toBeTruthy();
  });
});
