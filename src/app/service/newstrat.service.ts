import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Strat } from '../model/strat';

@Injectable({
  providedIn: 'root'
})
export class NewStratService {

  private tradeUrl: string;

  constructor(private http: HttpClient) {
    this.tradeUrl = environment.rest_host + '/strategyinput';
  }
  
  public makeNewStrat(Strat): void {
    this.http.post( environment.rest_host + '/strategyinput', Strat)
  }
}