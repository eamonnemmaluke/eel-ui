import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Strat } from '../model/strat';

@Injectable({
  providedIn: 'root'
})
export class StratnewService {
  private tradeUrl: string;

  constructor(private http: HttpClient) {
    this.tradeUrl = environment.rest_host + '/strategyinput';
  }
  
  public makeNewStrat(strat: Strat): Observable<Object> {
   
   let stratJson = {
     "id":-1,
     "stock":{
       "id":strat.stock.id
     },
     "size":strat.size,
     "closingPosition":strat.closingPosition
   }
   
   
    console.log('makenewstrat called ' + strat.id)
    console.log(strat.stock.id)
    console.log(strat.stock.ticker)
    console.log(strat.currentPosition)
    console.log(strat.size)
    console.log(strat.closingPosition)
    console.log(strat.lastTradePrice)
    console.log(strat.profit)
    console.log(strat.stopped)
    return this.http.post('http://localhost:8081/strategyinput', stratJson)
  }
   }

