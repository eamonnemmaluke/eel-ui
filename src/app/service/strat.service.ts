import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Strat } from '../model/strat';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StratService {

private tradeUrl: string;

constructor(private http: HttpClient) {
  this.tradeUrl = environment.rest_host + '/strategyinput';
}

public findAll(): Observable<Strat[]> {
  return this.http.get<Strat[]>(this.tradeUrl);
}
}
