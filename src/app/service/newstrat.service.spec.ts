import { TestBed } from '@angular/core/testing';

import { NewstratService } from './newstrat.service';

describe('NewstratService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewstratService = TestBed.get(NewstratService);
    expect(service).toBeTruthy();
  });
});
