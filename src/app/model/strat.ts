import { stock } from '../model/stock';

export class Strat {
    id: number;
    stock: stock;
    size: number;
    currentPosition: number;
    closingPosition: number;
    lastTradePrice: number;
    profit: number;
    stopped: Date;
  
  }