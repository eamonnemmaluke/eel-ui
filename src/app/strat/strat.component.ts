import { Observable, timer, Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { stock } from '../model/stock';
import { Strat } from '../model/strat';
import { StratService } from '../service/strat.service';
import { NewStratService } from '../service/newstrat.service';
import { StratnewService } from '../service/stratnew.service';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'app-strat',
  templateUrl: './strat.component.html',
  styleUrls: ['./strat.component.css']
})
export class StratComponent implements OnInit {

  private updateSubscription: Subscription;
  private newStrat: Strat;
  private Hstock: stock;
  strats: Strat[];
  newStock
  newAmount
  newPL

  constructor(private stratService: StratService, private stratnewService:StratnewService) {
    this.newStrat= new Strat()
    this.Hstock= new stock()
   }

  ngOnInit() {
    this.updateStrats();
  }

  ngOnDestroy() {
      this.updateSubscription.unsubscribe();
  }


  private updateStrats() {
  this.stratService.findAll().subscribe(data => {
      this.strats = data.reverse();
      console.log("loading strategies: ");
      console.log(this.strats[1].currentPosition);
      console.log(this.strats);
      this.startRefreshTimer();
    });
  }
    private startRefreshTimer() {
      this.updateSubscription = timer(10000).subscribe(
        (val) => { this.updateStrats() }
      );
    }

    newStratInit(){
      this.newStrat.id= -1;
      console.log("newStratInit was used")
      // this.newStrat.currentPosition=0;
      // this.newStrat.lastTradePrice=0;
      // this.newStrat.profit=0;
      // this.newStrat.stopped=null;
    }

    handleClick(){
      console.log("handleClick was used")
      console.log("A strat is born")
      this.newStratInit()
      // this.Hstock.ticker=null
      this.Hstock.id=this.newStock
      // this.newStrat.stock=this.Hstock
      this.newStrat.size=this.newAmount
      this.newStrat.closingPosition=this.newPL
      // this.newStrat.exitProfitLoss=7
      // this.newStrat.size=15
      // this.Hstock.id=2
      // this.Hstock.ticker='BCDE'
      // this.Hstock.ticker='AAPL'
      this.newStrat.stock=this.Hstock
      this.stratnewService.makeNewStrat(this.newStrat).subscribe((response)=>{console.log(response)})
    }
}
