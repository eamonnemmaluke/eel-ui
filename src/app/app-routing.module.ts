import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { StratComponent } from './strat/strat.component';
import { TradesComponent } from './trades/trades.component';
import { Trades2Component } from './trades2/trades2.component';

const routes: Routes = [
  {path:'Home',component:HomeComponent},
  {path:'Strat',component:StratComponent},
  {path:'TradesOld',component:TradesComponent},
  {path:'Trades',component:Trades2Component},
  {path:'**',redirectTo:'Home',pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
